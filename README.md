# Streptococcus Synthetic Data

The four simulated datasets used to compare our pipeline with MetaPhlAn2 were generated with InSilicoSeq [12] using the MiSeq model provided. These sets can be downloaded at: 
https://bitbucket.org/streptosynth/metagenome/downloads/

Truth sets:

Set	NCBI Accession	Taxon name	EzBioCloud name	Genome size	Truth

SET 01

	GCA_000164675.2	Streptococcus parasanguinis ATCC 15912	Streptococcus parasanguinis	2153652	15.00%

	GCA_000257765.1	Streptococcus anginosus subsp. whileyi CCUG 39159	Streptococcus anginosus subsp. whileyi	2294730	10.00%
	
	GCA_000187465.1	Streptococcus infantis ATCC 700779	Streptococcus infantis	1905984	5.00%
	
	GCA_000380005.1	Streptococcus didelphis DSM 15616	Streptococcus didelphis	1877438	5.00%
	
	GCA_000380025.1	Streptococcus entericus DSM 14446	Streptococcus entericus	2036468	15.00%
	
	GCA_000027165.1	Streptococcus mitis B6	FN568063_s	2146611	30.00%
	
	GCA_000185265.1	Streptococcus oralis ATCC 49296	GL622184_s	2068336	20.00%
	

SET 02

	GCA_000014485.1	Streptococcus thermophilus LMD-9	Streptococcus thermophilus	1864178	10.00%

	GCA_000007425.1	Streptococcus pyogenes MGAS315	Streptococcus pyogenes	1900521	15.00%
	
	GCA_000019025.1	Streptococcus pneumoniae Taiwan19F-14	Streptococcus pneumoniae	2112148	7.50%
	
	GCA_000007465.2	Streptococcus mutans UA159	Streptococcus mutans	2032925	7.50%
	
	GCA_000380045.1	Streptococcus marimammalium DSM 18627	Streptococcus marimammalium	1505444	10.00%
	
	GCA_000180055.1	Streptococcus downei F0415	Streptococcus downei	2239421	20.00%
	
	GCA_000463425.1	Streptococcus constellatus subsp. pharyngis C1050	Streptococcus constellatus subsp. pharyngis	1991156	5.00%
	
	GCA_000007265.1	Streptococcus agalactiae 2603V/R	Streptococcus agalactiae	2160267	5.00%
	
	GCA_000314795.2	Streptococcus sp. F0442	KB373315_s	2231248	20.00%
	
	
SET 03
	
	GCA_001578775.1	Streptococcus cristatus DD08	KQ969067_s	2206539	20.00%

	GCA_000722685.1	Streptococcus mitis SK667	JPFV_s	2136987	10.00%
	
	GCA_002005545.1	Streptococcus mitis 321A	LBMT_s	2110680	13.00%
	
	GCA_002096935.1	Streptococcus mitis B_5756_13	NCVM_s	1896604	5.00%
	
	GCA_001075675.1	Streptococcus oralis 918_SORA	JUNW_s	1884524	20.00%
	
	GCA_001579175.1	Streptococcus oralis DD24	KQ970764_s	2129793	13.00%
	
	GCA_002096595.1	Streptococcus oralis subsp. oralis OD_311844-09	NCUR_s	1951174	5.00%
	
	GCA_900012395.1	Streptococcus suis 9401240	CZEF_s	2174179	10.00%
	
	GCA_003934335.1	Streptococcus suis PP422	RSDO_s	2075657	4.00%
	
	
SET 04

	GCA_900095845.1	Streptococcus timonensis Marseille-P2915	Streptococcus timonensis	1925331	25.00%

	GCA_003595525.1	Streptococcus respiraculi HTS25	Streptococcus respiraculi	2067971	25.00%
	
	GCA_000423745.1	Streptococcus plurextorum DSM 22810	Streptococcus plurextorum	2103464	25.00%
	
	GCA_002953735.1	Streptococcus pluranimalium TH11417	Streptococcus pluranimalium	2065522	25.00%
	
